﻿using Moq;
using System;
using Task_3;
using Xunit;

namespace Task_3Tests
{
    public class Task_3Tests
    {
        [Fact]
        public void Add_Account_With_Correct_Data()
        {
            var mock = new Mock<IRepository<Account>>();

            IAccountService service = new AccountService(mock.Object);
            Account acc = new Account { FirstName = "Benny", LastName = "Benassi", BirthDate = new DateTime(1990, 01, 01) };
            service.AddAccount(acc);

            mock.Verify(repo => repo.Add(It.IsAny<Account>()));
        }

        [Fact]
        public void Add_Account_With_Incorrect_First_Name()
        {
            var mock = new Mock<IRepository<Account>>();

            IAccountService service = new AccountService(mock.Object);
            Account acc = new Account { FirstName = "", LastName = "Benassi", BirthDate = new DateTime(1990, 01, 01) };

            Assert.Throws<ArgumentNullException>(() => service.AddAccount(acc));
            Assert.Throws<MockException>(() => mock.Verify(repo => repo.Add(It.IsAny<Account>())));
        }

        [Fact]
        public void Add_Account_With_Incorrect_Last_Name()
        {
            var mock = new Mock<IRepository<Account>>();

            IAccountService service = new AccountService(mock.Object);
            Account acc = new Account { FirstName = "Benny", LastName = null, BirthDate = new DateTime(1990, 01, 01) };

            Assert.Throws<ArgumentNullException>(() => service.AddAccount(acc));
            Assert.Throws<MockException>(() => mock.Verify(repo => repo.Add(It.IsAny<Account>())));
        }

        [Fact]
        public void Add_Account_With_Incorrect_Birth_Date()
        {
            var mock = new Mock<IRepository<Account>>();

            IAccountService service = new AccountService(mock.Object);
            Account acc = new Account { FirstName = "Benny", LastName = "Benassi", BirthDate = new DateTime(2015, 01, 01) };

            Assert.Throws<ArgumentNullException>(() => service.AddAccount(acc));
            Assert.Throws<MockException>(() => mock.Verify(repo => repo.Add(It.IsAny<Account>())));
        }
    }
}
