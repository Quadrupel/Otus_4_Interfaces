﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Task_1
{
    /// <summary>
    /// Класс с реализацией IEnumerable<T>
    /// </summary>
    /// <typeparam name="T">Any generic type</typeparam>
    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        /// <summary>
        /// Поле с потоком
        /// </summary>
        private readonly Stream _stream;
        /// <summary>
        /// поле с сериализатором
        /// </summary>
        private readonly ISerializer<T> _serializer;

        /// <summary>
        /// конструктор класса
        /// </summary>
        /// <param name="stream">Поток для сериализатора</param>
        /// <param name="serializer">Сериализатор</param>
        public OtusStreamReader(Stream stream, ISerializer<T> serializer)
        {
            _stream = stream ?? throw new ArgumentNullException(nameof(stream));
            _serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
        }

        /// <summary>
        /// Реализация IDisposable
        /// </summary>
        public void Dispose()
        {
            _stream.Dispose();
        }

        /// <summary>
        /// Реализация IEnumerable<T>
        /// </summary>
        /// <returns>Коллекция элементов после десериализации</returns>
        public IEnumerator<T> GetEnumerator()
        {
            var content = _serializer.Deserialize<T[]>(_stream);
            foreach (T item in content)
            {
                yield return item; 
            }
        }

        /// <summary>
        /// Реализация IEnumerable<T>
        /// </summary>
        /// <returns>IEnumerator</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
