﻿using System;
using System.IO;

namespace Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //исходный массив объектов
            Person[] people = new Person[]
            {
                new Person
                {
                    Id = 1,
                    Name = "Владимир",
                    Surname = "Чухманов",
                    Age = 25
                },
                new Person
                {
                    Id = 2,
                    Name = "Иван",
                    Surname = "Соснин",
                    Age = 25
                },
                new Person
                {
                    Id = 3,
                    Name = "Кирилл",
                    Surname = "Семёнов",
                    Age = 26
                },
                new Person
                {
                    Id = 4,
                    Name = "Владимир",
                    Surname = "Лесников",
                    Age = 26
                },
                new Person
                {
                    Id = 5,
                    Name = "Сергей",
                    Surname = "Пермяков",
                    Age = 26
                },
            };


            //проверка сериализации
            Console.WriteLine("//проверка сериализации");
            OtusXmlSerializer<Person[]> serializer = new OtusXmlSerializer<Person[]>();
            string content = serializer.Serialize(people);
            Console.WriteLine(content);
            Console.WriteLine("---------------------------------");

            //проверка десериализации
            Console.WriteLine("//проверка десериализации");
            Person[] newPeople = serializer.Deserialize<Person[]>(GenerateStreamFromString(content));
            foreach (Person p in newPeople)
            {
                Console.WriteLine($"ID: {p.Id}, Name: {p.Name}, Surname: {p.Surname}, Age: {p.Age}");
            }
            Console.WriteLine("---------------------------------");

            //проверка ридера (тут реализован IEnumerable)
            Console.WriteLine("//проверка ридера");
            OtusXmlSerializer<Person> serializer2 = new OtusXmlSerializer<Person>();
            OtusStreamReader<Person> reader = new OtusStreamReader<Person>(GenerateStreamFromString(content), serializer2);
            foreach (Person p in reader)
            {
                Console.WriteLine($"ID: {p.Id}, Name: {p.Name}, Surname: {p.Surname}, Age: {p.Age}");
            }
            Console.WriteLine("---------------------------------");

            //метод для создания потока
            Stream GenerateStreamFromString(string content)
            {
                var stream = new MemoryStream();
                var writer = new StreamWriter(stream);
                writer.Write(content);
                writer.Flush();
                stream.Position = 0;
                return stream;
            }
        }
    }
}
