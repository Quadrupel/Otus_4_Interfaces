﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.IO;
using System.Xml;

namespace Task_1
{
    /// <summary>
    /// Интерфейс для сериализатора
    /// </summary>
    /// <typeparam name="T">Any generic type</typeparam>
    public interface ISerializer<T>
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream stream);
    }


    /// <summary>
    /// Класс с реализацией ISerializer<T>
    /// </summary>
    /// <typeparam name="T">Any generic type</typeparam>
    public class OtusXmlSerializer<T> : ISerializer<T>
    {
        /// <summary>
        /// Поле для хранения настроек сериализатора
        /// </summary>
        private static readonly XmlWriterSettings _xmlWriterSettings;

        /// <summary>
        /// Конструктор класса по умолчанию
        /// </summary>
        static OtusXmlSerializer()
        {
            _xmlWriterSettings = new XmlWriterSettings { Indent = true };
        }

        /// <summary>
        /// Десериализует поток
        /// </summary>
        /// <typeparam name="T">Any generic type</typeparam>
        /// <param name="stream">Поток для десериализации</param>
        /// <returns>Десериализованный объект</returns>
        public T Deserialize<T>(Stream stream)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
                .Type<T>()
                .UseAutoFormatting()
                .UseOptimizedNamespaces()
                .EnableImplicitTyping(typeof(T))
                .Create();

            return serializer.Deserialize<T>(stream);
        }

        /// <summary>
        /// Сериализует объект
        /// </summary>
        /// <typeparam name="T">Any generic type</typeparam>
        /// <param name="item">Объект для сериализации</param>
        /// <returns>Сериализованная строку с данными объекта</returns>
        public string Serialize<T>(T item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
                .Type<T>()
                .UseAutoFormatting()
                .UseOptimizedNamespaces()
                .EnableImplicitTyping(typeof(T))
                .Create();

            return serializer.Serialize(_xmlWriterSettings, item);
        }
    }
}
