﻿using System;
using System.IO;
using Task_1;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //исходный массив объектов
            Person[] people = new Person[]
            {
                new Person
                {
                    Id = 1,
                    Name = "Владимир",
                    Surname = "Чухманов",
                    Age = 25
                },
                new Person
                {
                    Id = 2,
                    Name = "Иван",
                    Surname = "Соснин",
                    Age = 25
                },
                new Person
                {
                    Id = 3,
                    Name = "Кирилл",
                    Surname = "Семёнов",
                    Age = 26
                },
                new Person
                {
                    Id = 4,
                    Name = "Владимир",
                    Surname = "Лесников",
                    Age = 26
                },
                new Person
                {
                    Id = 5,
                    Name = "Сергей",
                    Surname = "Пермяков",
                    Age = 26
                },
            };

            //подготовка данных для ридера
            OtusXmlSerializer<Person[]> serializer = new OtusXmlSerializer<Person[]>();
            string content = serializer.Serialize(people);

            //проверка сортировки
            Console.WriteLine("//проверка сортировки");
            OtusXmlSerializer<Person> serializer2 = new OtusXmlSerializer<Person>();
            OtusStreamReader<Person> reader = new OtusStreamReader<Person>(GenerateStreamFromString(content), serializer2);
            PersonSorter sorter = new PersonSorter();
            foreach (Person p in sorter.Sort(reader))
            {
                Console.WriteLine($"ID: {p.Id}, Name: {p.Name}, Surname: {p.Surname}, Age: {p.Age}");
            }
            Console.WriteLine("---------------------------------");

            //метод для создания потока
            Stream GenerateStreamFromString(string content)
            {
                var stream = new MemoryStream();
                var writer = new StreamWriter(stream);
                writer.Write(content);
                writer.Flush();
                stream.Position = 0;
                return stream;
            }
        }
    }
}