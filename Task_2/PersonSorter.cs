﻿using System.Collections.Generic;
using System.Linq;
using Task_1;

namespace Task_2
{
    /// <summary>
    /// Интерфейс для сортировки
    /// </summary>
    /// <typeparam name="T">Any generic type</typeparam>
    public interface IAlgorithm<T>
    {
        IEnumerable<T> Sort(IEnumerable<T> notSortedItems);
    }

    /// <summary>
    /// Класс с реализацией интерфейса сортировки для класса c реализованным IEnumerable<Person>
    /// </summary>
    public class PersonSorter : IAlgorithm<Person>
    {
        /// <summary>
        /// Возвращает отсортированную коллекцию со списком людей
        /// </summary>
        /// <param name="notSortedItems">Неотсортированный список людей</param>
        /// <returns>Отсортированный список людей</returns>
        public IEnumerable<Person> Sort(IEnumerable<Person> notSortedItems)
        {
            return notSortedItems.OrderBy(p => p.Age).ThenBy(p => p.Surname).ThenBy(p => p.Id);
        }
    }
}
