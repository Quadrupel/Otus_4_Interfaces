﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Task_3
{
    /// <summary>
    /// Класс с реализацией интерфейса IRepository<T>
    /// </summary>
    /// <typeparam name="T">Any type</typeparam>
    public class Repository<T> : IRepository<T>
    {
        /// <summary>
        /// Поле для хранения пути к файлу с данными
        /// </summary>
        private readonly string _path;

        /// <summary>
        /// Конструктор с путем по умолчанию
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        public Repository(string path = "items.csv")
        {
            _path = path;
            if (!File.Exists(path))
            {
                File.WriteAllText(path, string.Empty);
            }
        }

        /// <summary>
        /// Добавляет новый элемент в файл
        /// </summary>
        /// <param name="item">Добавляемый элемент</param>
        public void Add(T item)
        {
            using (StreamWriter writer = new StreamWriter(_path))
            using (CsvWriter csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecord<T>(item);
            }
        }

        /// <summary>
        /// Возвращает первый элемент из файла, соответвующий заданным условиям
        /// </summary>
        /// <param name="predicate">Заданные условия для выбора элемента</param>
        /// <returns>Элемент, соответствующий условиям</returns>
        public T GetOne(Func<T, bool> predicate)
        {
            using (StreamReader reader = new StreamReader(_path))
            using (CsvReader csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                return csv.GetRecords<T>().FirstOrDefault(predicate);
            }
        }

        /// <summary>
        /// Возвращает коллекцию всех элементов из файла
        /// </summary>
        /// <returns>Коллекция всех элементов из файла</returns>
        IEnumerable<T> IRepository<T>.GetAll()
        {
            using (StreamReader reader = new StreamReader(_path))
            using (CsvReader csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                foreach (var item in csv.GetRecords<T>())
                {
                    yield return item;
                }
            }
        }
    }

    /// <summary>
    /// Класс с реализацией интерфейса IAccountService
    /// </summary>
    public class AccountService : IAccountService
    {
        /// <summary>
        /// поле для хранения ссылки на репозиторий
        /// </summary>
        private readonly IRepository<Account> _repository;

        /// <summary>
        /// Конструктор с присвоением ссылки на репозиторий
        /// </summary>
        /// <param name="repository">Репозиторий, используемый в классе</param>
        public AccountService(IRepository<Account> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Добавляет новый аккаунт в файл
        /// </summary>
        /// <param name="account">Новый аккаунт</param>
        public void AddAccount(Account account)
        {
            if (string.IsNullOrEmpty(account?.FirstName))
            {
                throw new ArgumentNullException("Поле FirstName пустое");
            }
            if (string.IsNullOrEmpty(account?.LastName))
            {
                throw new ArgumentNullException("Поле LastName пустое");
            }
            if (account?.BirthDate.Date > DateTime.Now.AddYears(-18).Date)
            {
                throw new ArgumentNullException("Возраст меньше 18 лет");
            }
            _repository.Add(account);
        }
    }
}
