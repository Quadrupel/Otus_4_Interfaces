﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Account> accounts = new List<Account>
            {
                new Account
                {
                    FirstName = "Иван",
                    LastName = "Иванов",
                    BirthDate = new DateTime(2000, 1, 1)
                },
                new Account
                {
                    FirstName = "John",
                    LastName = "Smith",
                    BirthDate = new DateTime(2000, 12, 25)
                }
            };

            using (var writer = new StreamWriter("accounts.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(accounts);
            }
        }
    }
}
